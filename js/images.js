
import pageLogo from '../images/logo.svg';

import burgerMenu from '../images/icon-hamburger.svg';
import navBackgroundMobile from '../images/bg-pattern-mobile-nav.svg';
import iconClose from '../images/icon-close.svg'

import headerImageDesktop from '../images/image-intro-desktop.jpg'
import headerImageMobile from '../images/image-intro-mobile.jpg';

import headerImagePatternLeftMobile from '../images/bg-pattern-intro-left-mobile.svg';
import headerImagePatternRightMobile from '../images/bg-pattern-intro-right-mobile.svg';

import headerImagePatternRightDesktop from '../images/bg-pattern-intro-right-desktop.svg';
import headerImagePatternLeftDesktop from '../images/bg-pattern-intro-left-desktop.svg';


import snappyProcess from '../images/icon-snappy-process.svg';
import affordablePrices from '../images/icon-affordable-prices.svg';
import peopleFirst from '../images/icon-people-first.svg';

import weWorkSectionMobile from '../images/bg-pattern-how-we-work-mobile.svg';
import weWorkSectionDesktop from '../images/bg-pattern-how-we-work-desktop.svg';

import facebookLogo from '../images/icon-facebook.svg';
import twitterLogo from '../images/icon-twitter.svg';
import pinterestLogo from '../images/icon-pinterest.svg';
import instagramLogo from '../images/icon-instagram.svg';

import footerBackgroundMobile from '../images/bg-pattern-footer-mobile.svg';

const images = {
    headerImageMobile: document.querySelector('.header__image-mobile'),
    headerImageDesktop: document.querySelector('.header__image-desktop'),
    pageLogo: document.querySelector('.page-logo'),
    headerImagePatternLeftMobile: document.querySelector('.header__image-pattern-left-mobile'),
    headerImagePatternRightMobile: document.querySelector('.header__image-pattern-right-mobile'),
    snappyProcess: document.querySelector('#snappyProcess'),
    affordablePrices: document.querySelector('#affordablePrices'),
    peopleFirst: document.querySelector('#peopleFirst'),
    facebook: document.querySelector('#facebook'),
    twitter: document.querySelector('#twitter'),
    pinterest: document.querySelector('#pinterest'),
    instagram: document.querySelector('#instagram'),
    footerLogo: document.querySelector('.footer__logo'),
    burgerMenu: document.querySelector('.burger-menu'),
    iconClose: document.querySelector('.icon-close'),
    headerImagePatternRightDesktop: document.querySelector('.header__image-pattern-right-desktop'),
    headerImagePatternLeftDesktop: document.querySelector('.header__image-pattern-left-desktop')
}


images.headerImageMobile.src = headerImageMobile;
images.headerImageDesktop.src = headerImageDesktop;

images.pageLogo.src = pageLogo;
images.headerImagePatternLeftMobile.src = headerImagePatternLeftMobile;
images.headerImagePatternRightMobile.src = headerImagePatternRightMobile;
images.snappyProcess.src = snappyProcess;
images.affordablePrices.src = affordablePrices;
images.peopleFirst.src = peopleFirst;
images.facebook.src = facebookLogo;
images.twitter.src = twitterLogo;
images.pinterest.src = pinterestLogo;
images.instagram.src = instagramLogo;
images.footerLogo.src = pageLogo;
images.burgerMenu.src = burgerMenu;
images.iconClose.src = iconClose;
images.headerImagePatternRightDesktop.src = headerImagePatternRightDesktop;
images.headerImagePatternLeftDesktop.src = headerImagePatternLeftDesktop;



document.querySelector('.footer__socials-wrapper').style.backgroundImage = `url(${footerBackgroundMobile})`;
document.querySelector('.nav__list-mobile').style.backgroundImage = `url(${navBackgroundMobile})`;


const checkScreenSize = (imageMobile, imageDesktop) => {
    var screenWidth = window.innerWidth;
    if (screenWidth <= 768) {
        // Load mobile image
        document.querySelector('.section__image-pattern-work').style.backgroundImage = `url(${imageMobile})`;
    } else {
        // desktop image
        document.querySelector('.section__image-pattern-work').style.backgroundImage = `url(${imageDesktop})`;
    }
}

checkScreenSize(weWorkSectionMobile, weWorkSectionDesktop);

window.addEventListener('resize', () => {
    checkScreenSize(weWorkSectionMobile, weWorkSectionDesktop);
})
