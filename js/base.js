export const elements = {
    burgerMenu: document.querySelector('.burger-menu'),
    navList: document.querySelector('.nav__list-mobile'),
    closeIcon: document.querySelector('.icon-close')
}