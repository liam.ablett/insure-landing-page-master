import { elements } from './base';


elements.burgerMenu.addEventListener('click', () => {
    elements.navList.classList.add('js-active');
    elements.burgerMenu.style.display = 'none';
    elements.closeIcon.style.display = 'block';
})

elements.closeIcon.addEventListener('click', () => {
    elements.navList.classList.remove('js-active');
    elements.burgerMenu.style.display = 'block';
    elements.closeIcon.style.display = 'none';
})